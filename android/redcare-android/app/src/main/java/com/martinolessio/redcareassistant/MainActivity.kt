package com.martinolessio.redcareassistant

import android.app.Activity
import android.app.AlertDialog
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.media.MediaPlayer
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.speech.RecognizerIntent
import android.speech.tts.TextToSpeech
import android.speech.tts.TextToSpeech.OnInitListener
import android.speech.tts.UtteranceProgressListener
import android.util.Log
import android.view.*
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.squareup.moshi.Moshi
import kotlinx.android.synthetic.main.activity_main.*
import okhttp3.*
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.File
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import kotlin.math.floor


class MainActivity : AppCompatActivity() {

    val TAG = "LOG_TAG"

    private val REQUEST_RECORD_AUDIO_PERMISSION = 200
    private val REQ_CODE_SPEECH_INPUT = 100
    val JSON = "application/json; charset=utf-8".toMediaType()
    val client = OkHttpClient();
    private val moshi = Moshi.Builder().build()
    var textToSpeech: TextToSpeech? = null
    private var map: HashMap<String, String> = HashMap()

    private var sessionId = generateSessionId()

    private lateinit var mProgressDialog: AlertDialog

    private var currentSpeech = ""

    private var userName = "Mario"

    private val DIALOG_ENDPOINT = "http://redcare-assistant.duckdns.org:3000/api/tim/dialogflow"
    private val DISEASES_ENDPOINT = "http://redcare-assistant.duckdns.org:3000/api/tim/notifydiseases"

    fun generateSessionId(): String {
        val chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
        var sessionId = ""
        for (i in 0..31) {
            sessionId += chars[floor(Math.random() * chars.length).toInt()]
        }
        return sessionId
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        actionButton.setOnClickListener {
            startVoiceInput()
        }

        val ctx = this

        textToSpeech = TextToSpeech(this, OnInitListener { status ->
            if (status != TextToSpeech.ERROR) {
                //textToSpeech.setLanguage(Locale.ITALY);
                map = HashMap<String, String>()
                map.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "messageID")
                textToSpeech!!.setOnUtteranceProgressListener(object :
                    UtteranceProgressListener() {
                    override fun onStart(utteranceId: String) {
                        //Nothing to do
                        Log.i("onStart", "OnStart")
                        Handler(Looper.getMainLooper()).post(Runnable { //return response from here to update any UI
                            mProgressDialog = setProgressDialog(ctx, currentSpeech)!!
                        })
                    }

                    override fun onDone(utteranceId: String) {
                        Log.i("onStart", "OnDone")
                        Handler(Looper.getMainLooper()).post(Runnable { //return response from here to update any UI
                            mProgressDialog.dismiss()
                        })

                        startVoiceInput()
                        currentSpeech = ""
                    }

                    override fun onError(utteranceId: String) {
                        //Nothing to do
                        Log.i("onStart", "onError")
                        Handler(Looper.getMainLooper()).post(Runnable { //return response from here to update any UI
                            mProgressDialog.dismiss()
                        })
                        currentSpeech = ""
                    }
                })
            }
        })

        // HACKATON CODE!
        // We ask Nonno Mario how does he feel after 30 seconds from boot time
        // In a real world product, the asking time should be customized and scheduled
        // ONLY FOR DEMO PURPOSE

        Handler().postDelayed({
            askNonnoMario()
        }, 1000 * 30)

    }

    private fun startVoiceInput() {
        val intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
        intent.putExtra(
            RecognizerIntent.EXTRA_LANGUAGE_MODEL,
            RecognizerIntent.LANGUAGE_MODEL_FREE_FORM
        )
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault())
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Parlami!")
        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT)
        } catch (a: ActivityNotFoundException) {
            Log.e("Ex", a.toString())
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if(textToSpeech?.isSpeaking!!)
            textToSpeech?.stop()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        if(textToSpeech?.isSpeaking!!)
            textToSpeech?.stop()
    }

    fun sendTextToServer(text: String) {
        val jsonPayload = JsonObject()
        jsonPayload.addProperty("message", text)
        jsonPayload.addProperty("session", sessionId)

        val requestBody = Gson().toJson(jsonPayload).toRequestBody(JSON)
        val request: Request = Request.Builder()
            .url(DIALOG_ENDPOINT)
            .post(requestBody)
            .build()

        val ctx = this

        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                // Handle this
            }

            override fun onResponse(call: Call, response: Response) {
                val responseString =  response.body!!.string()
                Log.d(TAG, responseString)
                var outputFile : File? = null

                val jsonResponse = Gson().fromJson(responseString, JsonElement::class.java).asJsonObject

                val responseText = jsonResponse.getAsJsonObject("dialogFlow").getAsJsonObject("queryResult").get("fulfillmentText").asString
                val audioJson = jsonResponse.getAsJsonObject("dialogFlow").getAsJsonObject("outputAudio").get("data").asJsonArray

                if(audioJson.size() > 0){
                    val bytes = ByteArray(audioJson.size())
                    for (i in 0 until audioJson.size()) {
                        bytes[i] = (audioJson.get(i).asInt).toByte()
                    }

                    val outputDir: File = baseContext.cacheDir
                    outputFile = File.createTempFile("audio", "wav", outputDir)
                    outputFile.writeBytes(bytes)
                }

                // HACKATON CODE !!!!!
                if(responseText.startsWith("Buongiorno")){
                    // Extract username!
                    userName = responseText.split(" ")[1]
                }

                // Extract symptoms
                val contexts = jsonResponse.getAsJsonObject("queryResult").get("outputContexts").asJsonArray
                if(contexts.size() > 0){
                    val fields = contexts.get(0).asJsonObject.get("parameters").asJsonObject.get("fields").asJsonObject
                    if(fields.has("sintomi")){
                        var symptomsList = ArrayList<String>()
                        val symptoms = fields.get("sintomi").asJsonObject.get("listValue").asJsonObject.get("values").asJsonArray
                        for(obj in symptoms){
                            symptomsList.add(obj.asJsonObject.get("stringValue").asString)
                        }

                        sendDiseasestToServer(symptomsList)
                    }
                }


                Handler(Looper.getMainLooper()).post(Runnable { //return response from here to update any UI
                    showResponse(responseText, outputFile)
                })

            }
        })
    }

    fun sendDiseasestToServer(diseases: ArrayList<String>) {
        val jsonPayload = JsonObject()
        jsonPayload.add("diseases", Gson().toJsonTree(diseases))

        val requestBody = Gson().toJson(jsonPayload).toRequestBody(JSON)
        val request: Request = Request.Builder()
            .url(DISEASES_ENDPOINT)
            .post(requestBody)
            .build()

        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                // Handle this
            }

            override fun onResponse(call: Call, response: Response) {
                val responseString =  response.body!!.string()
                Log.d(TAG, responseString)
            }
        })
    }


    private fun showResponse(response: String, file: File?){
        // Toast.makeText(baseContext, response, Toast.LENGTH_SHORT).show()
        if(file != null){
            var mediaPlayer = MediaPlayer()
            mediaPlayer.setDataSource(file.toString())
            mediaPlayer.prepare()
            mediaPlayer.start()
        }else{
            speechText(response)
        }
    }

    fun askNonnoMario() {
        speechText("Ciao $userName come ti senti oggi?")
    }

    fun speechText(toSpeak: String?) {
        currentSpeech = toSpeak!!
        textToSpeech?.speak(toSpeak, TextToSpeech.QUEUE_ADD, map)
    }


    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            REQ_CODE_SPEECH_INPUT -> {
                if (resultCode == Activity.RESULT_OK && null != data) {
                    val result =
                        data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS)

                    //Toast.makeText(this, result[0], Toast.LENGTH_SHORT).show()

                    sendTextToServer(result[0])
                }
            }
        }
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    fun setProgressDialog(context: Context, text: String?): AlertDialog? {

        val llPadding = 30
        val ll = LinearLayout(context)
        ll.orientation = LinearLayout.HORIZONTAL
        ll.setPadding(llPadding, llPadding, llPadding, llPadding)
        ll.gravity = Gravity.CENTER
        var llParam = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        llParam.gravity = Gravity.CENTER
        ll.layoutParams = llParam

        val progressBar = ProgressBar(context)
        progressBar.isIndeterminate = true
        progressBar.setPadding(0, 0, llPadding, 0)
        progressBar.layoutParams = llParam

        llParam = LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        llParam.gravity = Gravity.CENTER
        val tvText = TextView(context)
        if(text != null)
            tvText.text = text
        else
            tvText.text = "Sto parlando ..."
        tvText.setTextColor(Color.parseColor("#000000"))
        tvText.textSize = 20f
        tvText.layoutParams = llParam

        ll.addView(progressBar)
        ll.addView(tvText)

        val builder = AlertDialog.Builder(context)
        builder.setCancelable(false)
        builder.setView(ll)

        var dialog = builder.create()
        dialog.show()
        val window = dialog.window
        if (window != null) {
            val layoutParams = WindowManager.LayoutParams()
            layoutParams.copyFrom(dialog.window!!.attributes)
            layoutParams.width = LinearLayout.LayoutParams.WRAP_CONTENT
            layoutParams.height = LinearLayout.LayoutParams.WRAP_CONTENT
            dialog.window!!.attributes = layoutParams
        }

        return dialog
    }


}