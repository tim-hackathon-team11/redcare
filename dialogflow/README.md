In questo repo c'e' tutto il codice riguardante la parte di Dialogflow del progetto.

In particolare ci sono:
- RedCare.zip dove ci sono gli Intent e le Entity. File zip ricavato direttamente facendo "export as zip" dentro il tab "import e export" di dialogflow.
- fulfillment_g_scoperta.zip che contiene il codice usato per gestire l'intent "g_scoperta" per gestire il fioco dell'anno della scoperta dell'America.
