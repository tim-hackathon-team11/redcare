const language = require('@google-cloud/language');
const client = new language.LanguageServiceClient();


async function analyzeSentiment(text) {

    const document = {
      content: text,
      language: "it",
      type: 'PLAIN_TEXT',
    };
    // Detects the sentiment of the text
    const [result] = await client.analyzeSentiment({document: document});
    const sentiment = result.documentSentiment;
    return sentiment;
}

module.exports = {analyzeSentiment}