var express = require('express');
var router = express.Router();
var request = require('request');

const dialogflow = require('@google-cloud/dialogflow');

const DialogFlow = require("../lib/dialogflow");
const Sentiment = require("../lib/sentiment");


const CONSTANTS = require('../lib/constants');
const {
    text
} = require('body-parser');

const CONFIGURATION = {
    username: "Angela",
    userAge: 82,
    alerts: {
        alertMethods: [
            "SMS",
            "EMAIL"
        ],
        alertContacts: [
            "YOUR_PHONE_NUMBER",
            "YOUR_EMAIL_ADDRESS"
        ],
        onInactivity: true,
        inactivityTimeout: 180,
        onLoliness: true,
        onQuizError: true,
        quizErrorRate: 5,
        onDisease: true,
        diseaseTriggers: [
            "vomito",
            "tosse"
        ]
    },
    reminders: [{
        name: "pastiglie",
        frequency: "daily",
        time: "16:00",
        recheckAfterMinutes: "30",
        recheckTimes: 2
    }]
};


function sendSMS(number, text, callback) {
    var headers = {
        'Content-Type': 'application/json',
        'apikey': CONSTANTS.APIKEY
    };

    var options = {
        url: CONSTANTS.SMS_ENDPOINT,
        method: 'POST',
        headers: headers,
        json: {
            address: "tel:+39" + number,
            message: text
        }
    };

    console.log(options);

    request(options, callback);
}

function sendEmail(recipient, text, callback) {

    console.log("Sorry, we are too late to write this function! ;) ");

    callback(null, {
        status: "OK"
    })
}


router.post('/tim/sms', function (req, res, next) {

    var message = req.body.message;
    var number = req.body.number;

    sendSMS(number, message, function (error, response, body) {

        console.log(error, body);

        if (error)
            return res.error(error);
        res.send(response);
    });
});

router.post('/tim/dialogflow', async function (req, res, next) {

    var message = req.body.message;
    var sessionId = req.body.session;

    let result = await DialogFlow.executeQuery(message, sessionId);
    let sentimentResult = await Sentiment.analyzeSentiment(message);

    var data = {
        dialogFlow: result,
        sentiment: sentimentResult
    }

    if (message.toLowerCase().indexOf("solitudine") !== -1) {
        if (sentimentResult.score < 0) {

            var message = "ALERT: " + CONFIGURATION.username + " ha il morale molto basso oggi (indice di tristezza: " + Math.floor(sentimentResult.score * -100) + "%). Magari sarebbe una buona idea fargli una telefonata.";

            sendSMS(CONFIGURATION.alerts.alertContacts[0], message, function (error, response, body) {
                // NOOP                            
            });
        }
    }

    res.send(data);

});

router.post('/tim/notifydiseases', async function (req, res, next) {

    // TODO: persist diseases log on some sort of database
    var SMSNotificationSent = false;
    var EmailNotificationSent = false;

    if (CONFIGURATION.alerts.onDisease) {
        var diseases = req.body.diseases;

        for (disease of diseases) {
            if (CONFIGURATION.alerts.diseaseTriggers.indexOf(disease) !== -1) {

                if (CONFIGURATION.alerts.alertMethods.indexOf("SMS") != -1 && SMSNotificationSent == false) {

                    SMSNotificationSent = true;

                    var message = "ALERT: " + CONFIGURATION.username + " ha i seguenti sintomi: " + diseases.join(", ");

                    sendSMS(CONFIGURATION.alerts.alertContacts[0], message, function (error, response, body) {

                        if (error)
                            return res.json({
                                status: "KO",
                                notificationSent: false,
                                error: error
                            });
                        res.json({
                            status: "OK",
                            notificationSent: true,
                            error: null
                        });
                    });
                }

                if (CONFIGURATION.alerts.alertMethods.indexOf("EMAIL") != -1 && EmailNotificationSent == false) {
                    EmailNotificationSent = true;

                    sendEmail(CONFIGURATION.alerts.alertContacts[1], message, function (error, response, body) {

                        if (error)
                            return res.json({
                                status: "KO",
                                notificationSent: false,
                                error: error
                            });
                        res.json({
                            status: "OK",
                            notificationSent: true,
                            error: null
                        });
                    });
                }
            }
        }



    }
    if (EmailNotificationSent == false && SMSNotificationSent == false)
        res.json({
            status: "OK",
            notificationSent: false,
            error: null
        });

});

router.get('/tim/configuration', async function (req, res, next) {

    res.json(CONFIGURATION);
});



module.exports = router;